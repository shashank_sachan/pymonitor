import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

def notification(subject=None, data=None, receivers='support_india@campuseai.org'):
    relay_server = 'smtp.gmail.com'
    port = 587
    sender = 'lgk.shashank@gmail.com'
    password = 'New@1234'
    receivers = receivers
    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = receivers
    msg['Subject'] = subject
    body = data
    msg.attach(MIMEText(body, 'plain'))

    try:
        server = smtplib.SMTP(relay_server, port)
        server.ehlo()
        server.starttls()
        server.login(sender, password)
        text = msg.as_string()
        server.sendmail(sender, receivers, text)
        server.quit()
        return True, "Successfully sent email - {0}".format(str(text))
    except Exception, e:
        return False, "Unable to send email - {0}".format(str(e))

#https://www.google.com/settings/security/lesssecureapps
#https://accounts.google.com/DisplayUnlockCaptcha