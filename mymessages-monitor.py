# Author
# Shashank Sachan
# Date : Mar 12th, 2017
# RUN: python mymessages-monitor.py
# Ubuntu: apt-get install python-dev libmysqlclient-dev; pip install MySQL-python
# Centos: yum install MySQL-python
#00 03 * * * /usr/bin/python /apps/mycampus/conf/pymonitor/mymessages-monitor.py

import MySQLdb as mdb
import sys
from collections import namedtuple
from send import notification
from datetime import datetime
from socket import gethostname
import urllib2
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('/apps/logs/tomcat/mymessages-monitor.log')
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

class ConnectionManager(object):
    def __init__(self):
        self.username  = 'root'
        self.password  = 'admin2008'
        self.campuseai = 'campuseai'
        self.server    = '127.0.0.1'

    def connect(self):
        try:
            conn = mdb.connect(self.server, self.username, self.password, self.campuseai)
            logger.info('DB connection established successfully.')
            return conn
        except Exception, e:
            logger.error('DB Connection Failed : {0}'.format(e))
            sys.exit()

    def disconnect(self, conn):
        conn.close()
        logger.info('DB Connection closed')


class MessageManager(object):
    def __init__(self, conn, older_than_days):
        self.older_than_days = older_than_days
        self.conn = conn

    def msg_older_than_x_days(self):
        message_ids = []
        query = "select id from alerts_message WHERE date < DATE_SUB(NOW(), INTERVAL {0} DAY)".format(self.older_than_days)
        cur = self.conn.cursor(mdb.cursors.DictCursor)
        logger.info('Executing Query : {0}'.format(query))
        cur.execute(query)
        for i in range(cur.rowcount):
            row = cur.fetchone()
            if row is None: continue
            message_ids.append(row['id'])
        return message_ids

    def alert_alerts_msgid_count(self, msg_id):
        query = "select count(*) from alerts_alert where message_id={0}".format(msg_id)
        cur = self.conn.cursor(mdb.cursors.DictCursor)
        #logger.info('Executing Query : {0}'.format(query))
        cur.execute(query)
        row = cur.fetchone()
        count = row['count(*)']
        logger.info('Count in alert_alerts for msg_id {0} is {1}'.format(msg_id, count))
        return count

    def total_rows_alerts_alert(self):
        query = "select count(*) from alerts_alert"
        cur = self.conn.cursor(mdb.cursors.DictCursor)
        logger.info('Executing Query : {0}'.format(query))
        cur.execute(query)
        row = cur.fetchone()
        count = row['count(*)']
        return count

    def get_expired_messages(self):
        current_date = datetime.now()
        message_ids = []
        query = "select message_id, value from alerts_message_attributes where name='expires'"
        cur = self.conn.cursor(mdb.cursors.DictCursor)
        logger.info('Executing Query : {0}'.format(query))
        cur.execute(query)
        for i in range(cur.rowcount):
            row = cur.fetchone()
            if row is None: continue
            date = datetime.strptime(row['value'].split()[0], '%Y-%m-%d')
            if date < current_date:
                message_ids.append(row['message_id'])
        return message_ids



try:
    #Setting variables
    #expired_rows_can_be_deleted = 0
    rows_can_be_deleted  = 0
    older_than_days = 90
    msg_threshold = 500000
    ip = urllib2.urlopen("http://169.254.169.254/latest/meta-data/public-ipv4").read()
    hostname = gethostname()

    #Connection to database
    db = ConnectionManager()
    conn = db.connect()

    #MyMessage Data Manipulation
    al = MessageManager(conn, older_than_days)
    total_rows = al.total_rows_alerts_alert()

    #Get details of the messages older than x days
    message_ids_x_days = al.msg_older_than_x_days()
    #for message_id in message_ids_x_days:
    #    count = al.alert_alerts_msgid_count(message_id)
    #    rows_can_be_deleted = rows_can_be_deleted +  count


    #Get details of the expired messages
    message_ids_expired = al.get_expired_messages()
    #for message_id in message_ids_expired:
    #    count = al.alert_alerts_msgid_count(message_id)
    #    expired_rows_can_be_deleted = expired_rows_can_be_deleted + count


    #Actual rows that can be deleted
    uniq_message_ids = list(set(message_ids_x_days)|set(message_ids_expired))
    logger.info("uniq_message_ids : {0}".format(uniq_message_ids))
    for message_id in uniq_message_ids:
        count = al.alert_alerts_msgid_count(message_id)
        rows_can_be_deleted = rows_can_be_deleted +  count


    #Logging Info
    logger.info("Total ID's older than {0} days & expired messages : {1}".format(older_than_days, len(uniq_message_ids)))
    logger.info("Total rows [alerts_alert]: {0}".format(total_rows))
    logger.info("Number of rows that can be deleted from alerts_alert: {0}".format(rows_can_be_deleted))

    if total_rows > msg_threshold:
        logger.info('Too many rows in alerts_alert table. Sending Email...')

        body = """
        Total ID's older than {0} days & expired messages : {1}
        Total rows [alerts_alert]: {2}
        Number of rows that can be deleted from alerts_alert: {3}
        """.format(older_than_days, len(uniq_message_ids), total_rows, rows_can_be_deleted)

        status, msg = notification('{0}({1}) - MyMessages Threshold Alert'.format(hostname, ip), body)
        if status: logger.info(msg)
        else: logger.error(msg)
    else:
        logger.info('Row count is fine in alerts_alert')

except Exception, e:
    logger.error(e)
finally:
    db.disconnect(conn)