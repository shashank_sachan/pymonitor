# Author
# Shashank Sachan
# Date : June 29th, 2017
# RUN: python ibdata.py
#00 01 * * * /usr/bin/python /apps/mycampus/conf/pymonitor/ibdata.py

from send import notification
from commands import getoutput
from socket import gethostname
import urllib2
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('/apps/logs/tomcat/ibdata-monitor.log')
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

def main():
    ip = urllib2.urlopen("http://169.254.169.254/latest/meta-data/public-ipv4").read()
    hostname = gethostname()
    threshold = 1572864
    breached = '\n'

    cmd = "du /apps/mysql/data/ibdata*"
    output = getoutput(cmd)
    for ibfile in output.split('\n'):
         size, name = ibfile.split('\t')
         size = size
         if size > threshold:
             breached = '{0} : {1}G\n'.format(name, float(size)/(1024*1024)) + breached 

    logger.info('Ib Data size : {0}'.format(breached))
    subject = '{0}({1}) - IB Data Threshold'.format(hostname, ip)
    data = breached

    if len(breached) > 0:
        status, msg = notification(subject, data)
        if status: logger.info(msg)
        else: logger.error(msg)
    else:
        logger.info('IB Data size is not high')

if __name__ == '__main__':
    main()