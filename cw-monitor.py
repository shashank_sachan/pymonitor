# Author
# Shashank Sachan
# Date : Mar 12th, 2017
# RUN: python cw-monitor.py
#*/15 * * * * /usr/bin/python /apps/mycampus/conf/pymonitor/cw-monitor.py

from commands import getoutput
from send import notification
from datetime import datetime
from socket import gethostname
import urllib2
import time
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('/apps/logs/tomcat/cw-monitor.log')
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

cw_limit = 150

def get_cw():
    cmd = 'netstat -an | grep -i close_wait | wc -l'
    current_cw = int(getoutput(cmd))
    logger.info('Current close wait connection : {0}'.format(current_cw))
    return current_cw

def main():
    current_cw = get_cw()
    ip = urllib2.urlopen("http://169.254.169.254/latest/meta-data/public-ipv4").read()
    hostname = gethostname()


    mytime = time.strftime("%I:%M %p")
    mydate = time.strftime("%A, %b %d, %Y")
    data = {
        'count' : current_cw,
        'datetime' : '{0}, {1}'.format(str(mytime), (mydate))
    }

    if current_cw > cw_limit:
        logger.info('Close wait connections are high. Sending Email...')
        status, msg = notification('{0}({1}) - Close Wait'.format(hostname, ip), 'Number of close_wait - {0}'.format(current_cw))
        if status: logger.info(msg)
        else: logger.error(msg)
    else:
        logger.info('Close wait connections are not high')

if __name__ == "__main__":
    main()